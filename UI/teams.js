exports.getTeamsList = getTeamsList;

var _ = require("underscore"),
    teams,
    teamsList = [
        {
            'id':'0',
            'name':'aaa',
            'captain':'Captain of 1st team',
            'coach':'Coach of 1st team',
            'achievements':'Some achievements'
        },
        {
            'id':'1',
            'name':'bbb',
            'captain':'Captain of 1st team',
            'coach':'Coach of 2nd team',
            'achievements':'Some achievements'
        },
        {
            'id':'2',
            'name':'ccc',
            'captain':'Captain of 3st team',
            'coach':'Coach of 3rd team',
            'achievements':'Some achievements'
        }
    ];


function getTeamsList () {
    return teams = _.clone(teamsList);
}