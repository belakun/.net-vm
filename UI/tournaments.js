exports.getTournamentsList = getTournamentsList;

var _ = require("underscore"),
    tournaments,
    clearTournamentList = [
        {
            'id':'0',
            'name':'SpringTour',
            'description':'BeachAndSun',
            'season':2014,
            'scheme':'1',
            'link': 'http://www.google.com',
			'applyingPeriodStart': '02.01.2014',			    
			'applyingPeriodEnd': '30.01.2014',		    
			'gamesStart': '01.02.2014',
			'gamesEnd': '01.05.2014',
			'transferStart': '01.02.2014',
			'transferEnd': '10.02.2014'		
        },
        {
            'id':'1',
            'name':'SummerJam',
            'description':'SeaAndSun',
            'season':2014,
            'scheme':'2',
            'link': 'http://www.google.com',
			'applyingPeriodStart': '02.01.2014',
			'applyingPeriodEnd': '30.01.2014',
			'gamesStart': '01.02.2014',
			'gamesEnd': '01.05.2014',
			'transferStart': '01.02.2014',
			'transferEnd': '10.02.2014'
        },
        {
            'id':'2',
            'name':'Winter StayAlive Chemp',
            'description':'BeachAndSun',
            'season':2014,
            'scheme':'2.5',
            'link': 'http://www.google.com',
			'applyingPeriodStart': '02.01.2014',
			'applyingPeriodEnd': '30.01.2014',
			'gamesStart': '01.02.2014',
			'gamesEnd': '01.05.2014',
			'transferStart': '01.02.2014',
			'transferEnd': '10.02.2014'
        },
        {
            'id':'3',
            'name':'Winter StayAlive Chemp for those who still alive yet and three chemps before',
            'description':'BeachAndSun',
            'season':2015,
            'scheme':'2.5',
            'link': 'http://www.google.com',
			'applyingPeriodStart': '02.01.2015',			    
			'applyingPeriodEnd': '30.01.2015',		    
			'gamesStart': '01.02.2015',
			'gamesEnd': '01.05.2015',
			'transferStart': '01.02.2015',
			'transferEnd': '10.02.2015'							
        },
        {
            'id':'4',
            'name':'Winter StayAlive Chemp for those who still alive yet and three chemps before',
            'description':'BeachAndSun',
            'season':2016,
            'scheme':'2.5',
            'link': 'http://www.google.com',
            'applyingPeriodStart': '02.01.2016',
            'applyingPeriodEnd': '30.01.2016',
            'gamesStart': '01.02.2016',
            'gamesEnd': '01.05.2016',
            'transferStart': '01.02.2016',
            'transferEnd': '10.02.2016'
        },
        {
            'id':'5',
            'name':'SpringTour',
            'description':'BeachAndSun',
            'season':2016,
            'scheme':'1',
            'link': 'http://www.google.com',
            'applyingPeriodStart': '02.01.2016',
            'applyingPeriodEnd': '30.01.2016',
            'gamesStart': '01.02.2016',
            'gamesEnd': '01.05.2016',
            'transferStart': '01.02.2016',
            'transferEnd': '10.02.2016'
        },
        {
            'id':'6',
            'name':'SummerJam',
            'description':'SeaAndSun',
            'season':2016,
            'scheme':'2',
            'link': 'http://www.google.com',
            'applyingPeriodStart': '02.01.2016',
            'applyingPeriodEnd': '30.01.2016',
            'gamesStart': '01.02.2016',
            'gamesEnd': '01.05.2016',
            'transferStart': '01.02.2016',
            'transferEnd': '10.02.2016'
        },
        {
            'id':'7',
            'name':'SpringTour',
            'description':'BeachAndSun',
            'season':2015,
            'scheme':'1',
            'link': 'http://www.google.com',
            'applyingPeriodStart': '02.03.2015',
            'applyingPeriodEnd': '30.03.2015',
            'gamesStart': '01.04.2015',
            'gamesEnd': '01.07.2015',
            'transferStart': '01.04.2015',
            'transferEnd': '10.04.2015'
        },
        {
            'id':'8',
            'name':'SummerJam',
            'description':'SeaAndSun',
            'season':2015,
            'scheme':'2',
            'link': 'http://www.google.com',
            'applyingPeriodStart': '02.03.2015',
            'applyingPeriodEnd': '30.03.2015',
            'gamesStart': '01.04.2015',
            'gamesEnd': '01.07.2015',
            'transferStart': '01.04.2015',
            'transferEnd': '10.04.2015'
        },
        {
            'id':'9',
            'name':'Winter StayAlive Chemp',
            'description':'BeachAndSun',
            'season':2015,
            'scheme':'2.5',
            'link': 'http://www.google.com',
            'applyingPeriodStart': '02.03.2015',
            'applyingPeriodEnd': '30.03.2015',
            'gamesStart': '01.04.2015',
            'gamesEnd': '01.07.2015',
            'transferStart': '01.04.2015',
            'transferEnd': '10.04.2015'
        }
    ];

function getTournamentsList () {
    return tournaments = _.clone(clearTournamentList);
}