var express = require('express'),
    _ = require("underscore"),
    app = express(),
    fs = require('fs'),
    bodyParser = require('body-parser'),
    initTournaments = require('./tournaments.js'),
    initUsers = require('./users.js'),
    initPlayers = require('./players.js'),
	initContributorInfo = require('./contributorsInfo.js'),
    initContributors = require('./contributors.js'),
    initTeams = require('./teams.js'),
    initMenu = require('./menuItems.js'),
    initContextMenu = require('./ContextMenuItems.js'),
    tournamentCollection = new initTournaments.getTournamentsList(),
    usersCollection = new initUsers.getUsersList(),
    playersCollection = new initPlayers.getPlayersList(),
	contributorsInfo = new initContributorInfo.getContributorsInfoList(),
    contributors = new initContributors.getContributorsList(),
    teamCollection =  new initTeams.getTeamsList(),
    menuItemsCollection = new initMenu.getMenuItemsList(),
    contextMenuItemsCollection = new initContextMenu.getContextMenuItemsList();


app.use('/client', express.static(__dirname + '/client'));
app.use('/js', express.static(__dirname + '/client/js'));
app.use('/css', express.static(__dirname + '/client/css'));
app.use('/lib', express.static(__dirname + '/client/js/lib'));
app.use('/app', express.static(__dirname + '/client/js/app'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

function OData (response) {
    return {
        value: response
    };
}

app.get('/', function (request, response) {
    response.sendFile('client/index.html', { root: __dirname });
});

app.get('/OData/Tournaments', function (request, response) {
    response.json(OData(parseArr(tournamentCollection)));
});

app.get('/OData/Users', function (request, response) {
    response.json(OData(parseArr(usersCollection)));
});

app.get('/OData/Players', function (request, response) {
    response.json(OData(parseArr(playersCollection)));
});

app.get('/OData/Teams', function (request, response) {
    response.json(OData(parseArr(teamCollection)));
});

app.get('/OData/About', function (request, response) {
    response.json(OData(contributors));
});

app.get('/OData/About/Contributors', function (request, response) {
    response.json(OData(contributorsInfo));
});

app.get('/OData/Menu', function (request, response) {
    response.json(OData(menuItemsCollection));
});

app.get('/OData/ContextMenu', function (request, response) {
    response.json(OData(contextMenuItemsCollection));
});

app.get('/reset', function (request, response) {
    response.redirect('/');
    tournamentCollection = new initTournaments.getTournamentsList();
    usersCollection = new initUsers.getUsersList();
    playersCollection = new initPlayers.getPlayersList();
    teamCollection = new initTeams.getTeamsList();
});

app.get('*', function (request, response) {
    function isRest () {
        var notRest = ['OData', '.css', '.js', '.map', '.eot', '.ttf', '.svg', '.woff', '.ico'],
            rest = true;
        
        notRest.forEach(function (key) {
            if (request.url.indexOf(key) !== -1) {
              rest = false;
            }
        });
        
        return rest;
    }
    
    if (isRest()) {
        response.sendFile('client/index.html', { root: __dirname });
    }
});

app.post('/OData/Tournaments', function (request, response) {
    var newTournament;
    
    newTournament = {
        id: tournamentCollection.length,
        name: request.param('name'),
        description: request.param('description'),
        season: request.param('season'),
        scheme: request.param('scheme'),
        link: request.param('link'),
		registrationStart: request.param('applyingPeriodStart'),
        registrationFinish: request.param('applyingPeriodEnd'),
        tournamentStart: request.param('gamesStart'),
        tournamentFinish: request.param('gamesEnd'),
        transferStart: request.param('transferStart'),
        transferFinish: request.param('transferEnd')
    };

    tournamentCollection.push(newTournament);
    response.json(OData(newTournament));
});

app.post('/OData/Users', function (request, response) {
    var newUser;
    
    newUser = {
        id: usersCollection.length,
        name: request.param('name'),
        email: request.param('email'),
        password: request.param('password'),
        confirmedPassword: request.param('confirmedPassword'),
        fullName: request.param('fullName'),
        cellPhone: request.param('cellPhone')
    };

    usersCollection.push(newUser);
    response.json(OData(newUser));
});

app.post('/OData/Teams', function (request, response) {
    var newTeam;

    newTeam = {
        'id': teamCollection.length,
        'name': request.param('name'),
        'captain': request.param('captain'),
        'coach': request.param('coach'),
        'achievements': request.param('achievements')
    };

    teamCollection.push(newTeam);
    response.json(OData(newTeam));
});

app.post('/OData/Players', function (request, response) {
    var newPlayer;

    newPlayer = {
        id: playersCollection.length,
        firstName: request.param('firstName'),
        lastName: request.param('lastName'),
        birthYear: request.param('birthYear'),
        teamId: request.param('teamId'),
        height: request.param('height'),
        weight: request.param('weight')
    };

    playersCollection.push(newPlayer);
    response.json(OData(newPlayer));
});

app.put('/OData/Tournaments(:id)', function (request, response) {
    var tournamentId = getId(request.params.id),
        editedTournament = tournamentCollection[tournamentId];
    
    for (key in editedTournament) {
        editedTournament[key] = request.param(key);
    }
  	editedTournament['id'] = tournamentId;
    response.json(OData(editedTournament));
});

app.put('/OData/Users(:id)', function (request, response) {
    var userId = getId(request.params.id),
        editedUser = usersCollection[userId];
    
    for (key in editedUser) {
        editedUser[key] = request.param(key);
    }
    editedUser['id'] = userId;
    response.json(OData(editedUser));
});


app.put('/OData/Teams(:id)', function (request, response) {
    var teamId = getId(request.params.id),
        editedTeam = teamCollection[teamId];

    for (key in editedTeam) {
        editedTeam[key] = request.param(key);
    }
    editedTeam['id'] = teamId;
    response.json(OData(editedTeam));
});


app.put('/OData/Players(:id)', function (request, response) {
    var playerId = getId(request.params.id),
        editedPlayer = playersCollection[playerId];

    for (key in editedPlayer) {
        editedPlayer[key] = request.param(key);
    }
    editedPlayer['id'] = playerId;
    response.json(OData(editedPlayer));
});

app.delete('/OData/Tournaments(:id)', function (request, response) {
    var tournamentId = getId(request.params.id);
    
    delete tournamentCollection[tournamentId];
	
	response.send();
});

app.delete('/OData/Users(:id)', function (request, response) {
    var userId = getId(request.params.id);

    delete usersCollection[userId];
	
	response.send();
});

app.delete('/OData/Teams(:id)', function (request, response) {
    var teamId = getId(request.params.id);

    delete teamCollection[teamId];

    response.send();
});

app.delete('/OData/Players(:id)', function (request, response) {
    var playerId = getId(request.params.id);

    delete playersCollection[playerId];
	
	response.send();
});

function getId (requestId) {
    return /\d+/.exec(requestId)[0];
}

function parseArr (arr) {
	var newArr = [],
		i;
	
	for (i = 0; i < arr.length; i++) {
		if (arr[i]) {
			newArr.push(arr[i]);
		}
	}
	
	return newArr;
}

app.listen(3000);

console.log("\nServer start on 127.0.0.1:3000\n");
