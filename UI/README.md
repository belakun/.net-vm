# README #

THIS IS BETA VERSION OF VolleyManagment App 

### Rules ###

* Something works wrong? DONT PUSH IT!
* Someones code need to be modified? WRITE TO AUTOR!

### Get Started ###

1. Create folder on your local drive and open it
2. using console, with active path to folder from step 1, initialize git service with command "git init"
3. then  use command "git remote add <local branch name> <url of repo(top-right corner link)>"
4. using command "git pull <local branch name> <repo branch name>" pull all files to your local repo
5. wanna make few changes? type "git add <changed file name>" then "git commit -m "commit text" "
6. push it! "git push <local branch name> <repo branch name>"

### Getting Started with Gulp

#### 1. Install gulp globally:

```
$ npm install --global gulp
```

#### 2. Install gulp and all plugins in project:

```
$ npm install
```

#### 3. Run gulp:

```
$ gulp
```

#### 4. Change links in dist/index.html files to absolute:

```
<link rel="stylesheet" href="/css/styles.css">
```

```
<script src="/js/scripts.js"></script>
```

#### 5. Start distServer:

```
$ node distServer.js











```