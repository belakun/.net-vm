exports.getUsersList = getUsersList;

var _ = require("underscore"),
    users,
    clearUsersList = [
        {
            'id':'0',
            'name':'Василий',
            'email':'www.khhh.com',
            'password':'23344343',
            'confirmPassword':'23344343',
            'fullName': 'Иванов Василий Николаевич',
            'cellPhone': '093 00 00 000'
        },
        {
            'id':'1',
            'name':'Al-Hubabi-Ibn-Zoom',
            'email':'',
            'password':'',
            'confirmPassword':'',
            'fullName': '',
            'cellPhone': ''
        },
        {
            'id':'2',
            'name':'Mao-Chi',
            'email':'',
            'password':'',
            'confirmPassword':'',
            'fullName': '',
            'cellPhone': ''
        },
        {
            'id':'3',
            'name':'Bond, James Bond',
            'email':'',
            'password':'',
            'confirmPassword':'',
            'fullName': '',
            'cellPhone': ''
        },
        {
            'id':'4',
            'name':'Frodo',
            'email':'',
            'password':'',
            'confirmPassword':'',
            'fullName': '',
            'cellPhone': ''
        },
		{
            'id':'5',
            'name':'Берегонд',
            'email':'www.Берегонд.com',
            'password':'23344343',
            'confirmPassword':'23344343',
            'fullName': 'Иванов Василий Николаевич',
            'cellPhone': '093 00 00 000'
        },
        {
            'id':'6',
            'name':'Элендил',
            'email':'',
            'password':'',
            'confirmPassword':'',
            'fullName': '',
            'cellPhone': ''
        },
        {
            'id':'7',
            'name':'Глорфиндел',
            'email':'',
            'password':'',
            'confirmPassword':'',
            'fullName': '',
            'cellPhone': ''
        },
        {
            'id':'8',
            'name':'Древобород',
            'email':'',
            'password':'',
            'confirmPassword':'',
            'fullName': '',
            'cellPhone': ''
        },
        {
            'id':'9',
            'name':'Радагаст',
            'email':'',
            'password':'',
            'confirmPassword':'',
            'fullName': '',
            'cellPhone': ''
        },
		{
            'id':'10',
            'name':'Саруман',
            'email':'www.Саруман.com',
            'password':'23344343',
            'confirmPassword':'23344343',
            'fullName': 'Иванов Василий Николаевич',
            'cellPhone': '093 00 00 000'
        },
        {
            'id':'11',
            'name':'Галадриэль',
            'email':'',
            'password':'',
            'confirmPassword':'',
            'fullName': '',
            'cellPhone': ''
        },
        {
            'id':'12',
            'name':'Арвен',
            'email':'',
            'password':'',
            'confirmPassword':'',
            'fullName': '',
            'cellPhone': ''
        },
        {
            'id':'13',
            'name':'Некромант',
            'email':'',
            'password':'',
            'confirmPassword':'',
            'fullName': '',
            'cellPhone': ''
        },
        {
            'id':'14',
            'name':'Арагорн',
            'email':'',
            'password':'',
            'confirmPassword':'',
            'fullName': '',
            'cellPhone': ''
        }
    ];    


function getUsersList () {
    return users = _.clone(clearUsersList);
}