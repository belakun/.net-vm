QUnit.module('Utils');
QUnit.test('setUp function', function (assert) {
    var app = {},
        result = {
            module1: {},
            module2: {}
        };

    /*test*/
    setUp(app, ['module1', 'module2']);

    assert.propEqual(app, result, 'Модули добавляются');
});