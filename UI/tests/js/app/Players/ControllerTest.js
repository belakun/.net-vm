QUnit.module('Players', {
    beforeEach: function () {
        $('body').append('<div id="main"></div>');
        this.tmp = App.Players.PlayerCollectionView;
        App.Players.PlayerCollectionView = function () {
            this.el = 'Players list';
            this.render = function () {
                return this;
            };

            return this;
        };
    },
    afterEach: function () {
        $('#main').remove();
        App.Players.PlayerCollectionView = this.tmp;
    }
});

QUnit.test('Controller function', function (assert) {
    var $el = $('#main'),
        vm = {
            mediator: new Mediator()
        },
        controller = new App.Players.Controller();

    /*test*/
    vm.mediator.publish('ShowPlayers');

    assert.equal($el.html(), 'Players list', 'Игроки отображаются');
});