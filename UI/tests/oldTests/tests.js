QUnit.module('Utils');
QUnit.test('setUp function', function (assert) {
    var app = {},
        result = {
            module1: {},
            module2: {}
        };

    /*test*/
    setUp(app, ['module1', 'module2']);

    assert.propEqual(app, result);
});

QUnit.module('Tournaments', {
    beforeEach: function () {
        $('body').append('<div id="main"></div>');
        this.tmp = App.Tournaments.TournamentCollectionView;
        App.Tournaments.TournamentCollectionView = function () {
            this.el = 'Tournaments list';
            this.render = function () {
                return this;
            };

            return this;
        };
    },
    afterEach: function () {
        $('#main').remove();
        App.Tournaments.TournamentCollectionView = this.tmp;
    }
});

QUnit.test('Controller function', function (assert) {
    var $el = $('#main'),
        vm = {
            mediator: new Mediator()
        },
        controller = new App.Tournaments.Controller();

    /*test*/
    vm.mediator.publish('ShowTournaments');

    assert.equal($el.html(), 'Tournaments list');
});

QUnit.module('Users', {
    beforeEach: function () {
        $('body').append('<div id="main"></div>');
        this.tmp = App.Users.UserCollectionView;
        App.Users.UserCollectionView = function () {
            this.el = 'Users list';
            this.render = function () {
                return this;
            };

            return this;
        };
    },
    afterEach: function () {
        $('#main').remove();
        App.Users.UserCollectionView = this.tmp;
    }
});

QUnit.test('Controller function', function (assert) {
    var $el = $('#main'),
        vm = {
            mediator: new Mediator()
        },
        controller = new App.Users.Controller();

    /*test*/
    vm.mediator.publish('ShowUsers');

    assert.equal($el.html(), 'Users list');
});