exports.getPlayersList = getPlayersList;

var _ = require("underscore"),
    players,
    clearPlayersList = [
        {
            'id':'0',            
            'firstName':'Дарт',
            'lastName':'Вейдер',
            'teamId': 0,
            'birthYear':'1989',
            'height':'181',
            'weight': '72'            
        },
        {
            'id':'1',            
            'firstName':'Люк',
            'lastName':'Скайуокер',
            'teamId': 1,
            'birthYear':'1990',
            'height':'180',
            'weight': '70'  
        },
        {
            'id':'2',            
            'firstName':'Оби-Ван',
            'lastName':'Кеноби',
            'teamId': 2,
            'birthYear':'1987',
            'height':'188',
            'weight': '78'  
        },
        {
            'id':'3',            
            'firstName':'Йода',
            'lastName':'Мастер',
            'teamId': 0,
            'birthYear':'1989',
            'height':'179',
            'weight': '71'  
        },
        {
            'id':'4',            
            'firstName':'Хан',
            'lastName':'Соло',
            'teamId': 1,
            'birthYear':'1991',
            'height':'170',
            'weight': '60'  
        },
		{
            'id':'5',            
            'firstName':'Граф',
            'lastName':'Дуку',
            'teamId': 2,
            'birthYear':'1989',
            'height':'181',
            'weight': '72'            
        },
        {
            'id':'6',            
            'firstName':'Дарт',
            'lastName':'Си́диус',
            'teamId': 0,
            'birthYear':'1990',
            'height':'180',
            'weight': '70'  
        },
        {
            'id':'7',            
            'firstName':'Нут',
            'lastName':'Ганрей',
            'teamId': 1,
            'birthYear':'1987',
            'height':'188',
            'weight': '78'  
        },
        {
            'id':'8',            
            'firstName':'Мейс',
            'lastName':'Винду',
            'teamId': 2,
            'birthYear':'1989',
            'height':'179',
            'weight': '71'  
        },
        {
            'id':'9',            
            'firstName':'Квай-Гон',
            'lastName':'Джинн',
            'teamId': 0,
            'birthYear':'1991',
            'height':'170',
            'weight': '60'  
        },
		{
            'id':'10',            
            'firstName':'Дарт',
            'lastName':'Мол',
            'teamId': 1,
            'birthYear':'1989',
            'height':'181',
            'weight': '72'            
        },
        {
            'id':'11',            
            'firstName':'Джа-Джа',
            'lastName':'Бинкс',
            'teamId': 2,
            'birthYear':'1990',
            'height':'180',
            'weight': '70'  
        },
        {
            'id':'12',            
            'firstName':'Чубакка',
            'lastName':'',
            'teamId': 0,
            'birthYear':'1987',
            'height':'188',
            'weight': '78'  
        },
        {
            'id':'13',            
            'firstName':'Асока ',
            'lastName':'Тано',
            'teamId': 1,
            'birthYear':'1989',
            'height':'179',
            'weight': '71'  
        },
        {
            'id':'14',            
            'firstName':'Падме',
            'lastName':'Амидала',
            'teamId': 2,
            'birthYear':'1991',
            'height':'170',
            'weight': '60'  
        }
    ];    


function getPlayersList () {
    return players = _.clone(clearPlayersList);
}