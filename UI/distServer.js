var express = require('express'),
    _ = require("underscore"),
    app = express(),
    fs = require('fs'),
    bodyParser = require('body-parser'),
    initTournaments = require('./tournaments.js'),
    initUsers = require('./users.js'),
    initPlayers = require('./players.js'),
    initContributors = require('./contributors.js'),
    tournamentCollection = new initTournaments.getTournamentsList(),
    usersCollection = new initUsers.getUsersList(),
    playersCollection = new initPlayers.getPlayersList(),
    contributors = new initContributors.getContributorsList(),
    groups = new initContributors.getGroupsList();

// For build
app.use('/dist', express.static(__dirname + '/dist'));
app.use('/scripts', express.static(__dirname + '/dist/scripts'));
app.use('/content', express.static(__dirname + '/dist/content'));
app.use('/fonts', express.static(__dirname + '/dist/content/fonts'));
app.use('/client/img', express.static(__dirname + '/dist/content/img'));

// For debug
app.use('/client', express.static(__dirname + '/client'));
app.use('/js', express.static(__dirname + '/client/js'));
app.use('/css', express.static(__dirname + '/client/css'));
app.use('/lib', express.static(__dirname + '/client/js/lib'));
app.use('/app', express.static(__dirname + '/client/js/app'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

function OData (response) {
    return {
        value: response
    };
}

app.get('/', function (request, response) {
    response.sendFile('dist/index.html', { root: __dirname });
});

app.get('/OData/Tournaments', function (request, response) {
    response.json(OData(parseArr(tournamentCollection)));
});

app.get('/OData/Users', function (request, response) {
    response.json(OData(parseArr(usersCollection)));
});

app.get('/OData/Players', function (request, response) {
    response.json(OData(parseArr(playersCollection)));
});

app.get('/OData/About/Group', function (request, response) {
    response.json(OData(groups));
});

app.get('/OData/About/Contributor', function (request, response) {
    response.json(OData(contributors));
});

app.get('/reset', function (request, response) {
    response.redirect('/');
    tournamentCollection = new initTournaments.getTournamentsList();
    usersCollection = new initUsers.getUsersList();
    playersCollection = new initPlayers.getPlayersList();
});

app.get('*', function (request, response) {
    function isRest () {
        var notRest = ['OData', '.css', '.js', '.map', '.eot', '.ttf', '.svg', '.woff'],
            rest = true;
        
        notRest.forEach(function (key) {
            if (request.url.indexOf(key) !== -1) {
              rest = false;
            }
        });
        
        return rest;
    }
    
    if (isRest()) {
        response.sendFile('dist/index.html', { root: __dirname });
    }
});

app.post('/OData/Tournaments', function (request, response) {
    var newTournament;
    
    newTournament = {
        id: tournamentCollection.length,
        name: request.param('name'),
        description: request.param('description'),
        season: request.param('season'),
        scheme: request.param('scheme'),
        link: request.param('link')
    };

    tournamentCollection.push(newTournament);
    response.json(OData(newTournament));
});

app.post('/OData/Users', function (request, response) {
    var newUser;
    
    newUser = {
        id: usersCollection.length,
        name: request.param('name'),
        email: request.param('email'),
        password: request.param('password'),
        confirmedPassword: request.param('confirmedPassword'),
        fullName: request.param('fullName'),
        cellPhone: request.param('cellPhone')
    };

    usersCollection.push(newUser);
    response.json(OData(newUser));
});

app.post('/OData/Players', function (request, response) {
    var newPlayer;

    newPlayer = {
        id: playersCollection.length,
        firstName: request.param('firstName'),
        lastName: request.param('lastName'),
        birthYear: request.param('birthYear'),
        height: request.param('height'),
        weight: request.param('weight')
    };

    playersCollection.push(newPlayer);
    response.json(OData(newPlayer));
});

app.put('/OData/Tournaments(:id)', function (request, response) {
    var tournamentId = getId(request.params.id),
        editedTournament = tournamentCollection[tournamentId];
    
    for (key in editedTournament) {
        editedTournament[key] = request.param(key);
    }
  	editedTournament['id'] = tournamentId;
    response.json(OData(editedTournament));
});

app.put('/OData/Users(:id)', function (request, response) {
    var userId = getId(request.params.id),
        editedUser = usersCollection[userId];
    
    for (key in editedUser) {
        editedUser[key] = request.param(key);
    }
    editedUser['id'] = userId;
    response.json(OData(editedUser));
});

app.put('/OData/Players(:id)', function (request, response) {
    var playerId = getId(request.params.id),
        editedPlayer = playersCollection[playerId];

    for (key in editedPlayer) {
        editedPlayer[key] = request.param(key);
    }
    editedPlayer['id'] = playerId;
    response.json(OData(editedPlayer));
});

app.delete('/OData/Tournaments(:id)', function (request, response) {
    var tournamentId = getId(request.params.id);
    
    delete tournamentCollection[tournamentId];
});

app.delete('/OData/Users(:id)', function (request, response) {
    var userId = getId(request.params.id);

    delete usersCollection[userId];
});

app.delete('/OData/Players(:id)', function (request, response) {
    var playerId = getId(request.params.id);

    delete playersCollection[playerId];
});

function getId (requestId) {
    return /\d/.exec(requestId)[0];
}

function parseArr (arr) {
	var newArr = [],
		i;
	
	for (i = 0; i < arr.length; i++) {
		if (arr[i]) {
			newArr.push(arr[i]);
		}
	}
	
	return newArr;
}

app.listen(3000);

console.log("\nServer start on 127.0.0.1:3000\n");
