exports.getContributorsInfoList = getContributorsInfoList;

var _ = require("underscore"),
    contrib,
    contributorsInfo =[
        {
             'id':'0',
             'firstName':'Олег',
             'lastName':'Коваленко',
             'contributorTeamId': '0'
         },
         {
             'id':'1',
             'firstName':'Станислав',
             'lastName':'Коваленко',
			 'contributorTeamId': '0'
         },
         {
             'id':'2',
             'firstName':'Екатерина',
			 'lastName':'Коваленко',
             'contributorTeamId': '0'
         },
         {
             'id':'3',
             'firstName':'Денис',
			 'lastName':'Коваленко',
             'contributorTeamId': '0'
         },
         {
             'id':'4',
             'firstName':'Евгений',
			 'lastName':'Коваленко',
             'contributorTeamId': '0'
         },
         {
             'id':'5',
             'firstName':'Олег',
			 'lastName':'Коваленко',
             'contributorTeamId': '1'
         },
         {
             'id':'6',
             'firstName':'Мария',
			 'lastName':'Коваленко',
             'contributorTeamId': '1'
         },
         {
             'id':'7',
             'firstName':'Виктория',
			 'lastName':'Коваленко',
             'contributorTeamId': '1'
         },
         {
             'id':'8',
             'firstName':'Александр',
			 'lastName':'Коваленко',
             'contributorTeamId': '1'
         },
         {
             'id':'9',
             'firstName':'Дмитрий',
			 'lastName':'Коваленко',
             'contributorTeamId': '1'
         },
         {
             'id':'10',
             'firstName':'Алексей',
			 'lastName':'Коваленко',
             'contributorTeamId': '1'
         }	,
         {
             'id':'11',
             'firstName':'Сергей',
			 'lastName':'Коваленко',
             'contributorTeamId': '2'
         }	,
         {
             'id':'12',
             'firstName':'Дмитрий',
			 'lastName':'Коваленко',
             'contributorTeamId': '2'
         }	,
         {
             'id':'13',
             'firstName':'Станислав',
			 'lastName':'Коваленко',
             'contributorTeamId': '2'
         }	,
         {
             'id':'14',
             'firstName':'Руслан',
			 'lastName':'Коваленко',
             'contributorTeamId': '2'
         }	,
        {
            'id':'15',
            'firstName':'Катерина',
            'lastName':'Овчаренко',
            'contributorTeamId': '3'
        },
        {
             'id':'16',
             'firstName':'Ольга',
			 'lastName':'Шафаренко',
             'contributorTeamId': '3'
         }		 ,
        {
            'id':'17',
            'firstName':'Даниил',
            'lastName':'Коростиенко',
            'contributorTeamId': '3'
        },
        {
            'id':'18',
            'firstName':'Егор',
            'lastName':'Дыхов',
            'contributorTeamId': '3'
        }
    ];

function getContributorsInfoList () {
    return contrib = _.clone(contributorsInfo);
}