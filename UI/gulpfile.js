'use strict'

/*
!IMPORTANT!

For build: 
1. gulp build
2. gulp replace

For debug:
1. gulp debug
2. gulp rename
*/

var gulp = require('gulp'),
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-minify-css'),
    filter = require('gulp-filter'),
    flatten = require('gulp-flatten'),
    clean = require('gulp-clean'),
    debug = require('gulp-debug'),
    rename = require("gulp-rename"),    
    replace = require('gulp-replace'),
    path = '../dotNet/VolleyManagement.UI';

// Build
gulp.task('build', ['renameForBuild', 'fonts', 'img']);

// Clean
gulp.task('clean', function () {
    return gulp.src(path, {read: false})
                .pipe(clean({force: true}));
});

// Concating and Minifing
gulp.task('ConcatAndMinify', ['clean'], function () {
    var assets = useref.assets();
    
    return gulp.src('client/*.html')
                .pipe(assets)
                .pipe(gulpif('*.js', uglify()))
                .pipe(gulpif('*.css', minifyCss()))
                .pipe(assets.restore())
                .pipe(useref())
                .pipe(gulp.dest(path));   
});

// Copy Fonts
gulp.task('fonts', ['clean'], function () {
    return gulp.src('client/css/fonts/*')
        .pipe(filter('**/*.{eot,svg,ttf,woff}'))
        .pipe(flatten())
        .pipe(gulp.dest(path + '/content/fonts'));
});

// Copy Images
gulp.task('img', ['clean'], function () {
    return gulp.src('client/img/*')
        .pipe(filter('*.jpg'))
        .pipe(flatten())
        .pipe(gulp.dest(path + '/content/img'));
});

// Rename index.html to _Layout.cshtml
gulp.task('renameForBuild', ['ConcatAndMinify'], function () {
    gulp.src(path + '/index.html')
        .pipe(rename('/Areas/WebAPI/Views/Shared/_Layout.cshtml'))
        .pipe(gulp.dest(path));        
    
    gulp.src(path + '/index.html', {read: false})
        .pipe(clean({force: true})); 
});

// Replace for '~' and anti-Cache
gulp.task('replace', function(){
    var hash = new Date().getTime();
    
    gulp.src([path + '/Areas/WebAPI/Views/Shared/_Layout.cshtml'])
        .pipe(replace('scripts.js', 'scripts.js?rev=' + hash))
        .pipe(replace('styles.css', 'styles.css?rev=' + hash))
        .pipe(replace('href="', 'href="~/'))
        .pipe(replace('src="', 'src="~/'))
        .pipe(gulp.dest(path + '/Areas/WebAPI/Views/Shared/'));
});  

// Debug
gulp.task('debug', function () {
    gulp.src('client/*.html')
        .pipe(debug())
        .pipe(gulp.dest(path + '/Areas/WebAPI/Views/Shared'));

    gulp.src('client/**/*.js')
        .pipe(debug())
        .pipe(gulp.dest(path + '/scripts'));

    gulp.src('client/**/*.css')
        .pipe(debug())
        .pipe(gulp.dest(path + '/content'));

    gulp.src('client/**/*.{eot,svg,ttf,woff}')
        .pipe(debug())
        .pipe(gulp.dest(path + '/content'));

    gulp.src('client/**/*.{jpg, gif}')
        .pipe(debug())
        .pipe(gulp.dest(path + '/content')); 
});

//Rename
gulp.task('rename', function () {    
    gulp.src(path + '/Areas/WebAPI/Views/Shared/index.html')
        .pipe(rename('/Areas/WebAPI/Views/Shared/_Layout.cshtml'))
        .pipe(gulp.dest(path));        
    
    gulp.src(path + '/Areas/WebAPI/Views/Shared/index.html', {read: false})
        .pipe(clean({force: true})); 
});

// Path replace
gulp.task('pathReplace', function () {    
    gulp.src(path + '/Areas/WebAPI/Views/Shared/_Layout.cshtml')
        .pipe(replace('href="', 'href="~/Content'))
        .pipe(replace("src='", "src='~/Scripts"))
        .pipe(gulp.dest(path + '/Areas/WebAPI/Views/Shared/'));
});


/*
index: ~/VolleyManagement.UI/Areas/WebAPI/Views/Shared/_Layout.cshtml
CSS: ~/VolleyManagement.UI/Content/
JS: ~/VolleyManagement.UI/Scripts/
*/