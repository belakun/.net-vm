'use strict';

(function (This) {
    This.User = Backbone.Model.extend({
        urlRoot: '/OData/Users',

        defaults: function () {
            return {
                'name': '',
                'email': '',
                'password': '',
                'confirmPassword': '',
                'fullName': '',
                'cellPhone': ''
            };
        }, 

        validation: {
            name: [{
                required: true,
                msg: 'Поле не может быть пустым'
            }, {
                maxLength: 60,
                msg: 'Поле не может содержать более 60 символов'
            }, {
                pattern: 'lettersOnly',
                msg: 'Поле должно содержать только буквы'
            }],
            email: {
                required: true,
                msg: 'Неправильный формат почтового ящика',
                pattern: 'email'
            },
            cellPhone: {
                maxLength: 10,
                msg: 'Указан недействительный номер телефона',
                pattern: 'digits'
            },
            password: {
                required: true,
                msg: 'Пароль и подтверждение пароля не совпадают'
            },
            confirmPassword: {
                required: true,
                msg: 'Пароль и подтверждение пароля не совпадают',
                equalTo: 'password'
            },
            fullName: {
                maxLength: 60,
                pattern: 'lettersOnly'
            }
        }
    });
})(App.Users);