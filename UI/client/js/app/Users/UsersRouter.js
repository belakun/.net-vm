'use strict';

(function (This)  {
    This.Router = Backbone.Router.extend({
        routes: {
            'WebApi/Users': 'getUsers',
            'WebApi/Users/new': 'createUser',
            'WebApi/Users/:id/edit': 'editUser',
            'WebApi/Users/:id': 'getUser',
            'WebApi/Users*path': 'notFound'
        },
        
        initialize: function () {
            this.controller = new App.Users.Controller();
            //URL navigation
            vm.mediator.subscribe('CreateUser', this.navigateNewUser, null, this);
            vm.mediator.subscribe('ShowUsers', this.navigateShowUsers, null, this);
            vm.mediator.subscribe('EditUser', this.navigateEditUser, null, this);
            vm.mediator.subscribe('EditUserById', this.navigateEditUserById, null, this);
            vm.mediator.subscribe('ShowUserInfo', this.navigateShowUserInfo, null, this);
            vm.mediator.subscribe('ShowUserById', this.navigateShowUserById, null, this);
            vm.mediator.subscribe('UserViewClosed', this.navigateUserViewClosed, null, this);

            Backbone.history.loadUrl(Backbone.history.fragment);
        },

        navigateNewUser: function () {
            this.navigate('WebApi/Users/new');
        },

        navigateShowUsers: function () {
            this.navigate('WebApi/Users');
        },

        navigateEditUser: function (user) {
            this.navigate('WebApi/Users/' + user.id + '/edit');
        },

        navigateEditUserById: function (userId) {
            this.navigate('WebApi/Users/' + userId + '/edit');
        },

        navigateShowUserInfo: function (user) {
            this.navigate('WebApi/Users/' + user.id);
        },
       
        navigateShowUserById: function (userId) {
            this.navigate('WebApi/Users/' + userId);
        },

        navigateUserViewClosed: function (reason, id) {
            if (reason === 'afterCreating') {
                this.navigate('WebApi/Users');
            } else {
                this.navigate('WebApi/Users/' + id);
            };
        },

        getUsers: function () {
            vm.mediator.publish('ShowUsers');
        },

        getUser: function (id) {
            vm.mediator.publish('ShowUserById', id);
        },

        createUser: function () {
            vm.mediator.publish('CreateUser');
        },

        editUser: function (id) {
            vm.mediator.publish('EditUserById', id);
        },

        notFound: function () {
            vm.mediator.publish('Show404View');
        }
    });
})(App.Users);