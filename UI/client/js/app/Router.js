'use strict';

(function (This)  {
    This.Router = Backbone.Router.extend({
        routes: {
            '': 'index',
            'WebApi': 'index',
            'WebApi/': 'index',
            'WebApi/Home/': 'index',  
            'WebApi/Home': 'index',           
            'WebApi/Tournaments*path': 'tournaments',
            'WebApi/Users*path': 'users',
            'WebApi/Players*path': 'players',
            'WebApi/About*path': 'about',
            'WebApi/Teams*path': 'teams',
            'WebApi/*path': 'notFound'
        },
		
        initialize: function () {
            //URl navigation
            vm.mediator.subscribe('MainPageSelected', this.navigateMainPage, null, this);
            vm.mediator.subscribe('MenuItemSelected', this.navigateMenuItem, null, this);
        },

        navigateMainPage: function () {
            this.navigate('WebApi/Home', {trigger: true});
        },

        navigateMenuItem: function (pathname) {
            this.navigate('WebApi/' + pathname, {trigger: true});
        },

        index: function () {
            vm.subRouters['Tournaments'] || (vm.subRouters['Tournaments'] = new App.Tournaments.Router());
        },

        tournaments: function () {
            vm.subRouters['Tournaments'] || (vm.subRouters['Tournaments'] = new App.Tournaments.Router());
        },

        users: function () {
            vm.subRouters['Users'] || (vm.subRouters['Users'] = new App.Users.Router());
        },

        players: function () {
            vm.subRouters['Players'] || (vm.subRouters['Players'] = new App.Players.Router());
        },

        about: function () {
            vm.subRouters['About'] || (vm.subRouters['About'] = new App.About.Router());
        },

        teams: function () {
            vm.subRouters['Teams'] || (vm.subRouters['Teams'] = new App.Teams.Router());
        },

        notFound: function () {
            vm.mediator.publish('Show404View');
        }
    });
})(App);