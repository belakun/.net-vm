var error404Tpl = _.template([
    '<div class="row  error-container">',
        '<div class="col-md-6">',
            '<img class="error404-image img-circle" src="/client/img/4.jpg">',
        '</div>',
        '<div class="col-md-5 error-text-container">',
            '<h1>Аут!</h1></br></br>',
            '<h3>Вы запросили несуществующую страницу.</h3>',
        '</div>',
    '</div>'
].join(''));