'use strict';

(function (This) {
    This.Error404View = Backbone.View.extend({
        tagName: 'div',
        className: 'error-404',

        template: error404Tpl,

        render: function () {
            this.$el.html(this.template());

            return this;
        }
    });
})(App.ServicePages);