'use strict';

(function (This)  {
    This.Controller = function () {
        var $service = $('#main'),
            router,
			view;

        //vm.mediator.subscribe('ShowAbout', showAbout);
        vm.mediator.subscribe('Show404View', showErrorPage);

        //function showAbout () {
        //    hideAll();
        //    view = new This.GroupCollectionView();
        //
        //    router.navigate('About');
        //    $service.append(view.render().el);
        //}

        function showErrorPage () {
            hideAll();
            view = new This.Error404View();
            
            $service.append(view.render().el);
        }

        function hideAll () {
            $service.children().addClass('hidden');
        }
		
		this.setRouter = function (_router) {
			router = _router;
			return this;
		};

        return this;
    }
})(App.ServicePages);