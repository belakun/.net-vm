'use strict';

(function (This)  {
    This.Router = Backbone.Router.extend({
        routes: {
            'WebApi/About': 'showAbout'            
        },

        initialize: function () {
            var controller = new This.Controller();

            //URL navigation
            vm.mediator.subscribe('ShowAbout', this.navigateAbout, null, this);
            
            Backbone.history.loadUrl(Backbone.history.fragment);
        },

        navigateAbout: function () {
            this.navigate('WebApi/About');
        },

        showAbout: function () {
            vm.mediator.publish('ShowAbout');
        }
    });
})(App.About);