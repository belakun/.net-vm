var groupCollectionTpl = _.template([
    '<div class="panel panel-default group-collection">',
        '<div class="panel-heading">',
            '<div class="row">',
                '<div class="col-md-9 col-sm-8">',
                    '<h4>',
                        '<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span> ',
                        'Над проектом работали ',
                        '<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>',
                    '</h4>',
                '</div>',
            '</div>',
        '</div>',
        '<div class="list-group-container row">',
            
        '</div>',
    '</div>'
].join(''));