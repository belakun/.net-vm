"use strict";

(function (This) {
    This.ContributorCollection = Backbone.Collection.extend({

        url: '/OData/About/Contributors',

        model: This.Contributor

    });

})(App.About);