var contextMenuItemsTpl = _.template([
'<span class="<%= attributes.icon %>">',
'</span>',
'<a>',
'    <%= attributes.name %>',
'</a>'
].join(''));