'use strict';

(function (This)  {
    This.Router = Backbone.Router.extend({
        routes: {
            '': 'getGeneralPage',
            'WebApi': 'getGeneralPage',  
            'WebApi/': 'getGeneralPage',
            'WebApi/Home': 'getGeneralPage',   
            'WebApi/Home/': 'getGeneralPage',         
            'WebApi/Tournaments': 'getTournaments',
            'WebApi/Tournaments/new': 'createTournament',
            'WebApi/Tournaments/:id/edit': 'editTournament',
            'WebApi/Tournaments/:id': 'getTournament',
            'WebApi/Tournaments*path': 'notFound'
        },

        initialize: function () {
            var controller = new App.Tournaments.Controller();

            //URL navigation
            vm.mediator.subscribe('ShowTournaments', this.navigateTournaments, null, this);
            vm.mediator.subscribe('ShowTournamentInfo', this.navigateShowTournament, null, this);
            vm.mediator.subscribe('ShowTournamentById', this.navigateShowTournamentById, null, this);
            vm.mediator.subscribe('CreateTournament', this.navigateNewTournament, null, this);
            vm.mediator.subscribe('EditTournament', this.navigateEditTournament, null, this);
            vm.mediator.subscribe('EditTournamentById', this.navigateEditTournamentById, null, this);
            
            Backbone.history.loadUrl(Backbone.history.fragment);
        },

        navigateTournaments: function () {
            this.navigate('WebApi/Tournaments');
        },

        navigateShowTournament: function (tournament) {
            this.navigate('WebApi/Tournaments/' + tournament.id);
        },

        navigateShowTournamentById: function (id) {
            this.navigate('WebApi/Tournaments/' + id);
        },

        navigateNewTournament: function () {
            this.navigate('WebApi/Tournaments/new');
        },

        navigateEditTournament: function (tournament) {
            this.navigate('WebApi/Tournaments/' + tournament.id + '/edit');
        },

        navigateEditTournamentById: function (id) {
            this.navigate('WebApi/Tournaments/' + id + '/edit');
        },
        
        getGeneralPage: function () {
            this.navigate('WebApi/Home');
            vm.mediator.publish('GeneralPage loaded');
        },

        getTournaments: function () {
            vm.mediator.publish('ShowTournaments');
        },

        createTournament: function () {
            vm.mediator.publish('CreateTournament');
        },

        editTournament: function (id) {
            vm.mediator.publish('EditTournamentById', id);
        },

        getTournament: function (id) {
            vm.mediator.publish('ShowTournamentById', id);
        },

        notFound: function () {
            vm.mediator.publish('Show404View');
        }
    });
})(App.Tournaments);




