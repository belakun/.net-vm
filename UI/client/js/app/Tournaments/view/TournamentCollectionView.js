'use strict';

(function (This) {
    This.TournamentCollectionView = Backbone.View.extend({
        tagName: 'div',
        className: 'tournament-collection',

        template: tournamentCollectionTpl,

        events: {
            'click button.create': 'create'
        },

        initialize: function () {
            vm.mediator.subscribe('TournamentSaved', this.saveModel, {}, this);
            vm.mediator.subscribe('ShowNavMenu:Tournament', this.render, {}, this);
            vm.mediator.subscribe('Tournament', this.addOne, {}, this);
        },

        saveModel: function (model) {
            this.collection.add(model);
			this.update();
        },

        update: function () {
            this.render();

            vm.mediator.publish('addNavigator', {collection: this.collection, channel: 'Tournament'}, null, this);
        },

        render: function (tpl) {
            this.$el.html(this.template());
            if (tpl) {this.$el.append(tpl)}

            return this;
        },

        addOne: function (tournament) {
            var view = new This.TournamentView({model: tournament});
            
            this.$('.list-group').append(view.render().el);
        },

        create: function () {
            vm.mediator.publish('CreateTournament');
        },

        show: function () {
            this.$el.removeClass('hidden');
            vm.mediator.publish('addNavigator', {collection: this.collection, channel: 'Tournament'}, null, this);
        },

        hide: function () {
            this.$el.addClass('hidden');
        },

        getModelById: function (id, callback) {
            if (this.collection.get(id)) {
                callback(this.collection.get(id));
            } else {
                this.collection.once('sync', function () {
                    if (this.collection.get(id)) {
                        callback(this.collection.get(id));
                    } else {
                        vm.mediator.publish('Show404');
                    }
                }, this);
            }
        }
    });
})(App.Tournaments);