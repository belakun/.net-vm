"use strict";

(function (This) {
    This.TournamentDatePickersView = Backbone.View.extend({
        template: TournamentDatePickersTpl,
		dp: {},

		events : {
		   "dp.change .leftCalendar": "onChangeLeft",
		   "dp.change .rightCalendar": "onChangeRight",
		},
		
		onChangeLeft: function (e) {
			var hash = {
				'applyingPeriodStart': $(this.dp['applyingPeriodEnd']),
			    'gamesStart': $(this.dp['gamesEnd']),
			    'transferStart': $(this.dp['transferEnd'])
			};
			
			this.setLeftDPickerHandler(e, e.target.id, hash[e.target.id]);
		},

		onChangeRight: function (e) {
			this.setRightDPickerHandler(e.target.id);
		},
		
		initialize: function () {
		    this.model = this.model || new This.Tournament(); 
            this.modelBinder = new Backbone.ModelBinder();
            
            this.convertToDate();

            Backbone.Validation.bind(this);    
        },
		
        render: function (itaName) {
            this.$el.html(this.template(this.model.toJSON()));
            this.modelBinder.bind(this.model, this.$el);
			
            return this;
        },
		
		setHendlers: function () {
		    var $datePickers = this.$el.find(".datePicker");
			
			_.forEach($datePickers, function(datePicker) {
			    var datePickerName = datePicker.id;

			    this.dp[datePickerName] = datePicker;
				$(this.dp[datePickerName]).datetimepicker(this.setDPickerHash(datePickerName));
			}, this);
		},
        
		setDPickerHash: function (attrName) {
		   return {
				pickTime: false, 
				language: 'ru', 
			    defaultDate: this.model.get(attrName)
		   }
		},
		
		setLeftDPickerHandler: function (e, attrName, $dtp) {
			var val = this.$el.find("input[name='"+attrName+"']").val(),
				dateVal = this.toDate(val);
		  	
			this.model.set(attrName, dateVal);
			$dtp.data("DateTimePicker").setMinDate(e.date);
		},
		
		setRightDPickerHandler: function (attrName) {
			var val = this.$el.find("input[name='"+attrName+"']").val(),
				dateVal = this.toDate(val);

			this.model.set(attrName, dateVal);
		},
		toDate: function (strDate) {
            var arrItemDate = strDate.split('.'),
                day = Number(arrItemDate[0]),
                month = Number(arrItemDate[1]) - 1,
                year = Number(arrItemDate[2]);

            return (new Date(year, month, day)).toString();
        },
        convertToDate: function () {
            var dateAttributes = [
                'applyingPeriodStart',
                'applyingPeriodEnd',
                'gamesStart',
                'gamesEnd',
                'transferStart',
                'transferEnd'
                ],
                newDateAttributes = {};

            _.each(this.model.attributes, function (value, attribute) {
                if (dateAttributes.indexOf(attribute) !== -1) {
                    var arrItemDate = value.split('.'),
                        day = Number(arrItemDate[0]),
                        month = Number(arrItemDate[1]) - 1,
                        year = Number(arrItemDate[2]);

                    newDateAttributes[attribute] = (new Date(year, month, day)).toString(); 
                }
            });

            this.model.set(newDateAttributes);


        }
    });
})(App.Tournaments);