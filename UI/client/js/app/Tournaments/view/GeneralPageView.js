'use strict';

(function (This) {
    This.GeneralPageView = Backbone.View.extend({
        tagName: 'div',
        className: 'general-page',

        template: generalPageTpl,

        initialize: function () {
            this.listenToOnce(this.collection, 'sync', function () {
                vm.mediator.publish('Collection loaded');
            });

            this.collection.fetch();
        },

        render: function () {
            var groupsTournaments = this.selectGroupsTournaments(),
                i;

            this.$el.html(this.template());

            for (i = 0; i < 3; i++) {
                this.addOne(groupsTournaments.currentTournaments[i], 'list-group-now');
                this.addOne(groupsTournaments.futureTournaments[i], 'list-group-future');
                this.addOne(groupsTournaments.pastTournaments[i], 'list-group-past');
            }

            return this;
        },

        /**
         * Selects the current, future and past tournaments
         *
         * @method selectGroupsTournaments
         * @return {Object} Hash with 3 sorting arrays [currentTournaments, futureTournaments, pastTournaments]
         */
        selectGroupsTournaments: function () {
            var currentDate = new Date(),
                currentTournaments = [],
                futureTournaments = [],
                pastTournaments = [];

            this.collection.forEach(function (tournament) {
                var gamesStart = tournament.get('gamesStart'),
                    gamesEnd = tournament.get('gamesEnd');

                if (currentDate >= gamesStart && currentDate <= gamesEnd) {
                    currentTournaments.push(tournament);
                } else if (currentDate < gamesStart) {
                    futureTournaments.push(tournament);
                } else {
                    pastTournaments.push(tournament);
                }
            }, this);

            this.sortArr(currentTournaments, 'gamesEnd');
            this.sortArr(futureTournaments, 'gamesStart');
            this.sortArr(pastTournaments, 'gamesEnd');

            return {
                currentTournaments: currentTournaments,
                futureTournaments: futureTournaments,
                pastTournaments: pastTournaments
            }
        },

        /**
         * Convert string like 'day.month.year' to Date
         *
         * @method toDate
         * @param {String} strDate String like 'day.month.year'
         * @return {Date} converted Date
         */
        // toDate: function (strDate) {
        //     var arrItemDate = strDate.split('.'),
        //         day = Number(arrItemDate[0]),
        //         month = Number(arrItemDate[1]) - 1,
        //         year = Number(arrItemDate[2]);

        //     return new Date(year, month, day);
        // },


        toDate: function (strDate) {
            var arrItemDate = strDate.split('.'),
                day = Number(arrItemDate[0]),
                month = Number(arrItemDate[1]) - 1,
                year = Number(arrItemDate[2]);

            return new Date(year, month, day);
        },


        /**
         * Sorting array by gamesStart(gamesEnd)
         *
         * @method sortArr
         * @param {Array} arr Array of TournamentObjects
         * @param {String} item 'gamesStart'('gamesEnd') of tournamentObject on which will be sorting
         */
        sortArr: function (arr, item) {
            arr.sort(function (a, b) {
                var result = 0;

                if (a[item] < b[item]) {
                    result = -1;
                } else if (a[item] > b[item]) {
                    result = 1;
                }

                return result;
            });
        },

        /**
         * Added a tournament in the appropriate section
         *
         * @method addOne
         * @param {Object} tournament TournamentObject which need rendering
         * @param {String} cssClass CSS class ['list-group-now', 'list-group-future', 'list-group-future']
         */
        addOne: function (tournament, cssClass) {
            if (tournament) {
                var view = new This.TournamentView({model: tournament});

                this.$('.' + cssClass).append(view.render().el);
            }
        },

        show: function () {
            this.$el.removeClass('hidden');
        },

        hide: function () {
            this.$el.addClass('hidden');
        }
    });
})(App.Tournaments);