'use strict';

(function (This)  {
    This.Tournament = Backbone.Model.extend({
        urlRoot: '/OData/Tournaments',

        defaults: function () {
            var now = new Date(),
			    day, year, month,
                registrationStartDate, registrationFinishDate,
				tournamentStartDate, tournamentFinishDate,
				transferStartDate, transferFinishDate;

				day = this.formatValue(now.getDate());
				month = this.formatValue(now.getMonth() + 1);
				year = this.formatValue(now.getFullYear());

			 	registrationStartDate = day + '.' + month + '.' + year;
				registrationFinishDate = day + '.' + this.formatValue(now.getMonth() + 2) + '.' + year;						
				tournamentStartDate = day + '.' + this.formatValue(now.getMonth() + 2) + '.' + year;
				tournamentFinishDate = day + '.' + this.formatValue(now.getMonth() + 5) + '.' + year;
				transferStartDate = day + '.' + this.formatValue(now.getMonth() + 3) + '.' + year;
				transferFinishDate = day + '.' + this.formatValue(now.getMonth() + 4) + '.' + year;

            return {
                name: '',
                description: '',
                season: year,
                scheme: '1',
                regulationsLink: '',
				applyingPeriodStart: registrationStartDate,
				applyingPeriodEnd: registrationFinishDate,						
				gamesStart: tournamentStartDate,
				gamesEnd: tournamentFinishDate,
				transferStart: transferStartDate,
				transferEnd: transferFinishDate
            };
        },

        validation: {
            name: [{
                required: true,
                msg: 'Поле не может быть пустым'
            }, {
                maxLength: 60,
                msg: 'Поле не может содержать более 60 символов'
            }],
            description: {
                maxLength: 300,
                msg: 'Поле не может содержать более 300 символов',
            },
            season: {
                required: true,
                msg: 'Поле не может быть пустым'
            },
            scheme: {
                required: true,
                msg: 'Поле не может быть пустым'
            },            
			applyingPeriodStart: {
                required: true,
                msg: 'Поле не может быть пустым'
            },
			applyingPeriodEnd: {
                required: true,
                msg: 'Поле не может быть пустым'
            },			
			gamesStart: {
                required: true,
                msg: 'Поле не может быть пустым'
            },			
			gamesEnd: {
                required: true,
                msg: 'Поле не может быть пустым'
            },
			
			applyingPeriodStart: 
			  function(value) { 
				  var msg = '';
				  
				  if (value !== '') {				  
					  if (this.getDaysDiff(value, this.get('gamesStart')) > 90) {
							  msg = 'Регистрация раньше 3 месяцев чем начало турнира';
							  return msg;
					  }
					  if (this.getDaysDiff(value, this.get('applyingPeriodEnd')) <= 0) {
							  msg = 'Начало регистрации должно быть раньше конца';
							  return msg;
					  }
					  if (this.getDaysDiff(value, this.get('gamesStart')) === 0) {
							  msg = 'Начало регистрации и начало турнира совпадают';
							  return msg;
					  }
				  }
				  
			  },
			
			gamesStart: 
			  function(value) { 
				  var msg = '';
	  
				  if (value !== '') {
					  if (this.getDaysDiff(value, this.get('gamesEnd')) <= 0) {
							  msg = 'Начало турнира должно быть раньше конца турнира';
							  return msg;
					  }
					  if (this.getDaysDiff(this.get('applyingPeriodStart'), value) > 90) {
							  msg = 'Регистрация раньше 3 месяцев чем начало турнира';
							  return msg;
					  }					  
					  if (this.getDaysDiff(value, this.get('applyingPeriodStart')) === 0) {
							  msg = 'Начало регистрации и начало турнира совпадают';
							  return msg;
					  }

			      }
			  },
			
			transferStart: 
			  function(value) { 
				  var msg = '';
	  
				  if (value !== '') {
					  if (this.getDaysDiff(value, this.get('transferEnd')) <= 0) {
							  msg = 'Начало трансферного окна должно быть раньше конца';
							  return msg;
					  }
			      }
			  }
        },
		
		getDaysDiff: function (date1, date2){
		var msecPerMinute, msecPerHour,
			msecPerDay, firstDate, secondDate, 
			interval, days;

			msecPerMinute = 1000 * 60;
			msecPerHour = msecPerMinute * 60;
			msecPerDay = msecPerHour * 24;

			firstDate = new Date(date1);
			secondDate = new Date(date2);

			interval = firstDate.getTime() - secondDate.getTime();
			days = Math.floor(interval / msecPerDay );

			return days;
		},
		
		formatValue: function(value) {
			return (value < 10)? ('0' + value): value; 
		}
    });
})(App.Tournaments);












