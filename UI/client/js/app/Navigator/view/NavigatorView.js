'use strict';

(function (This) {
    This.NavigatorView = Backbone.View.extend({
        tagName: 'div',
        className: '',

        template: navigatorTpl,

        navIndex: (sessionStorage.nav)? JSON.parse(sessionStorage.nav): {},
        sizeList: [5, 10, 20, 50],

        events: {
            'click .return': 'render',
            'click .dropdown-element': 'setResultsAmount',
            'keyup .searchField': 'startSearch',
            'click .pageEl': 'addOne'
        },

        initialize: function (input) {
            this.collection = input.collection;
            this.channel = input.channel;
            this.pageSize = input.pageSize || 10;
            this.render();
        },

        render: function () {
            var pageCount = Math.ceil(this.collection.length / this.pageSize),
                startPosition,
                finishPosition,
                i;

            this.$el.html(this.template({
                pageSize: this.pageSize,
                sizeList: this.sizeList,
                pageCount: pageCount
            }));

            vm.mediator.publish('ShowNavMenu:' + this.channel, this.$el, null, this);

            this.index = this.navIndex[this.channel] || 0;

            startPosition = this.index * this.pageSize;
            finishPosition = startPosition + this.pageSize;

            //figure out if our collection has model with № startPosition
            if(!this.collection.models[startPosition]){
                startPosition = startPosition - this.pageSize || 0;
                this.index = 0;
            }

            for (i = startPosition; i < finishPosition; i++) {

                if (this.collection.models[i]) {
                    vm.mediator.publish(this.channel, this.collection.models[i], null, this);
                }
            }

            this.$(".pagination li").eq(this.index).addClass('active');
            this.delegateEvents();
            this.$('.searchField').focus();
            return this;
        },

        setResultsAmount: function (e) {
            this.pageSize = e.currentTarget.value;

            this.render();
        },

        startSearch: function () {
            var searchRequest = this.$('.searchField').val();

            if (searchRequest !== '') {
                this.filter(searchRequest);
            } else {
                this.render()
            }
            this.delegateEvents();
            this.$('.searchField').focus();
        },

        addOne: function (e) {
            this.navIndex[this.channel] = e.currentTarget.value - 1;
            this.render();
        },

        filter: function (value) {
            var param = [];

            function getFilters (channel) {
                var filters = {
                    'User': ['name'],
                    'Tournament': ['name'],
                    'Player': ['firstName','lastName'],
                    'Team': ['name']
                };

                return filters[channel];
            }

            param = getFilters(this.channel);

            var filtered = this.collection.filter(function (model) {
                var flag = false;
                param.forEach(function(paramName){
                    if (model.get(paramName).toLowerCase().indexOf(value.toLowerCase()) >= 0) {
                        flag = true
                    }
                });
                return flag;
            });

            this.$el.html(this.template({pageSize: this.pageSize, sizeList: this.sizeList, pageCount: 0}));
            vm.mediator.publish('ShowNavMenu:' + this.channel, this.$el, null, this);

            for (var i = 0; i < filtered.length; i++) {
                vm.mediator.publish(this.channel, filtered[i], null, this);
            }

            vm.mediator.publish('Notice', 'success', 'Найдено ' + filtered.length + ' результатов');
            this.$('.searchField').val(value);
        },

        removeMe: function () {
            sessionStorage.nav = JSON.stringify(this.navIndex);
            this.remove();
        }

    });
})(App.Navigator);