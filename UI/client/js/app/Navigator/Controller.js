'use strict';

(function (This) {
    This.Controller = function () {
        var navigatorView;

        setUpMediator();

        function setUpMediator () {
            vm.mediator.subscribe('addNavigator', addNavigator, {}, this);
        }

        function addNavigator (collection) {
            navigatorView && navigatorView.removeMe();
            navigatorView = new This.NavigatorView(collection);
        }

      return this;
    }
})(App.Navigator);