var navigatorSearchTpl = _.template([
    '<div class="input-group">',
        '<span class="input-group-btn return">',
            '<button class="btn btn-info" type="button">Назад</button>',
        '</span>',
        '<input type="text" class="form-control searchField" autofocus placeholder="Имя" aria-describedby="basic-addon1">',
        '<span class="input-group-btn">',
            '<button class="btn btn-success search" type="submit">Найти</button>',
        '</span>',
    '</div>',
].join(''));