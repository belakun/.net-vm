'use strict';

(function (This) {
    This.Controller = function () {

        var $menuId = $('#main-menu');

        start();

        function start () {
            setUpMediator();
            setUpMenu();
        }

        function setUpMediator () {
            vm.mediator.subscribe('Pressed menu item', show);
        }

        function setUpMenu () {
            var menu = new This.MenuView();

            $menuId.html(menu.render().$el);
        }

        function show (pathname) {
            if (location.pathname !== pathname) {
                vm.mediator.publish('MenuItemSelected', pathname);
            }
        }

        return this;
    }
})(App.Menu);