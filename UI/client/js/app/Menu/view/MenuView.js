'use strict';

(function (This) {
    This.MenuView = Backbone.View.extend({
        tagName: 'ul',
        className: 'list-menu nav nav-pills nav-stacked',
        menuItemsList: [
        {
            'id': '0',
            'path': '',
            'name': 'Главная'
        },
        {
            'id': '1',
            'path': 'Tournaments',
            'name': 'Турниры'
        },
        {
            'id': '2',
            'path': 'Users',
            'name': 'Пользователи'
        },
        {
            'id': '3',
            'path': 'Teams',
            'name': 'Команды'
        },
        {
            'id': '4',
            'path': 'Players',
            'name': 'Игроки'
        },
        {
            'id': '5',
            'path': 'About',
            'name': 'О нас'
        }
    ],
        initialize: function () {
            this.collection = new This.MenuItemsCollection(this.menuItemsList);
        },

        render: function () {
            this.collection.forEach(function (model) {
                var menuItem = new This.MenuItemView();
                this.$el.append(menuItem.render(model).$el);
            }, this);

            return this;
        }
    });
})(App.Menu);