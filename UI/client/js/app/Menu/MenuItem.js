"use strict";

(function (This)  {
    This.MenuItem = Backbone.Model.extend({
        defaults: {
            item: '',
            name: ''
        }
    });
})(App.Menu);