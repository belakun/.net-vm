'use strict';

(function (This) {
    This.PlayerCollectionView = Backbone.View.extend({
        tagName: 'div',
        className: 'playerCollection',

        template: playerCollectionTpl,

        events: {
            'click button.create': 'create'
        },

        initialize: function () {
            this.collection = new This.PlayerCollection();

            vm.mediator.subscribe('PlayerSaved', this.saveModel, {}, this);
			vm.mediator.subscribe('ShowNavMenu:Player', this.render, {}, this);
            vm.mediator.subscribe('Player', this.addOne, {}, this);

            this.listenToOnce(this.collection, 'sync', this.update);

            this.collection.fetch();
        },

        saveModel: function (model) {
            this.collection.add(model);
			this.update();
        },

        update: function () {
			this.render();

            vm.mediator.publish('addNavigator', {collection: this.collection, channel: 'Player'}, null, this);
        },

        addOne: function (player) {
            var view = new This.PlayerView({model: player});

            this.$('.list-group').append(view.render().el);
        },

        render: function (tpl) {
            this.$el.html(this.template());
            if (tpl) {this.$el.append(tpl)}

            return this;
        },

        create: function () {
            vm.mediator.publish('CreatePlayer');
        },

        show: function () {
            this.$el.removeClass('hidden');
            vm.mediator.publish('addNavigator', {collection: this.collection, channel: 'Player'}, null, this);
        },

        hide: function () {
            this.$el.addClass('hidden');
        },

        getModelById: function (id, callback) {
            if (this.collection.get(id)) {
                callback(this.collection.get(id));
            } else {
                this.collection.once('sync', function () {
                    if (this.collection.get(id)) {
                        callback(this.collection.get(id));
                    } else {
                        vm.mediator.publish('Show404');
                    }
                }, this);
            }
        }
    });
})(App.Players);