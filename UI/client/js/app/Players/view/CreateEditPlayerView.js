'use strict';

(function (This) {
    This.CreateEditView = Backbone.View.extend({
        tagName: 'div',

        template: editPlayerTpl,

        events: {
            'click .save': 'save',
            'click .cancel': 'cancel',
            'blur input': 'preValidate'
        },

        initialize: function () {
            this.model = this.model || new This.Player();
            this.defaultModelJSON = this.model.toJSON();
            this.modelBinder = new Backbone.ModelBinder();

            Backbone.Validation.bind(this);

            vm.mediator.subscribe('ShowPlayerById', this.undoChanges, {}, this);
        },

        render: function () {
            var teams = new App.Teams.TeamCollection();

            teams.once('sync', function () {
                var player = this.model.toJSON(),
                    playerTeams = teams.toJSON();

                this.$el.append(this.template({player: player, teams: playerTeams}));

                this.modelBinder.bind(this.model, this.el);
            }, this);

            teams.fetch()



            return this;
        },

        preValidate: function (e) {
            var attrName, error, validationResult,
                errors = {};
            if (e) {
                attrName = e.target.name;
                error = this.model.preValidate(attrName, this.model.get(attrName)
                );

                if (error) {
                    vm.mediator.publish('Hint', error, this.$('[name=' + attrName + ']'));
                }

                validationResult = error;
            } else {
                errors = this.model.preValidate({
                    firstName: this.model.get('firstName'),
                    lastName: this.model.get('lastName'),
                    birthYear: this.model.get('birthYear'),
                    height: this.model.get('height'),
                    weight: this.model.get('weight')
                });

                if (errors) {
                    for (attrName in errors) {
                        vm.mediator.publish('Hint', errors[attrName], this.$('[name=' + attrName + ']'));
                    }
                }
                validationResult = errors;
            }

            return validationResult;
        },

        save: function () {
            var isNewModel = this.model.isNew();

            if (!this.preValidate()) {
                // Don't showing model in CollectionView if model not saved on server
                this.model.once('sync', function () {
                    if (isNewModel) {
                        vm.mediator.publish('PlayerSaved', this.model);
                    }

                    vm.mediator.publish(
                        'Notice',
                        'success',
                        isNewModel? 'Вы успешно зарегистрировались!': 'Информация успешно изменена!'
                    );
                }, this);

                this.model.save();

                vm.mediator.publish(
                    'PlayerViewClosed',
                    isNewModel? 'afterCreating': 'afterEditing',
                    this.model.id
                );
            }
        },

        cancel: function () {
            this.undoChanges();
            vm.mediator.publish(
                'PlayerViewClosed',
                this.model.isNew()? 'afterCreating': 'afterEditing',
                this.model.id
            );
        },

        undoChanges: function () {
            this.modelBinder.unbind();
            this.model.off('change', this.preValidate);
            this.model.set(this.defaultModelJSON);
        }
    });
})(App.Players);