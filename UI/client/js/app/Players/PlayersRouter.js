'use strict';

(function (This)  {
    This.Router = Backbone.Router.extend({
        routes: {
            'WebApi/Players': 'getPlayers',
            'WebApi/Players/new': 'createPlayer',
            'WebApi/Players/:id/edit': 'editPlayer',
            'WebApi/Players/:id': 'getPlayer',
            'WebApi/Players/*path': 'notFound'
        },
        
        initialize: function () {
            this.controller = new App.Players.Controller();

            //URL navigation
            vm.mediator.subscribe('ShowPlayers', this.navigatePlayers, null, this);
            vm.mediator.subscribe('CreatePlayer', this.navigateCreate, null, this);
            vm.mediator.subscribe('PlayerViewClosed', this.navigatePlayerViewClosed, null, this);
            vm.mediator.subscribe('ShowPlayerInfo', this.navigateShowPlayer, null, this);
            vm.mediator.subscribe('ShowPlayerById', this.navigateShowPlayerById, null, this);
            vm.mediator.subscribe('EditPlayer', this.navigateEditPlayer, null, this);
            vm.mediator.subscribe('EditPlayerById', this.navigateEditPlayerById, null, this);
            
            Backbone.history.loadUrl(Backbone.history.fragment);
        },

        navigatePlayers: function () {
            this.navigate('WebApi/Players');
        },

        navigateCreate: function () {
            this.navigate('WebApi/Players/new');
        },

        navigatePlayerViewClosed: function (reason, id) {
            if (reason === 'afterCreating') {
                this.navigate('WebApi/Players');
            } else {
                this.navigate('WebApi/Players/' + id);
            }
        },

        navigateShowPlayer: function (player) {
            this.navigate('WebApi/Players/' + player.id);
        },

        navigateShowPlayerById: function (playerId) {
            this.navigate('WebApi/Players/' + playerId);
        },

        navigateEditPlayer: function (player) {
            this.navigate('WebApi/Players/' + player.id + '/edit');
        },

        navigateEditPlayerById: function (playerId) {
             this.navigate('WebApi/Players/' + playerId + '/edit');
        },

        getPlayers: function () {
            vm.mediator.publish('ShowPlayers');
        },

        getPlayer: function (id) {
            vm.mediator.publish('ShowPlayerById', id);
        },

        createPlayer: function () {
            vm.mediator.publish('CreatePlayer');
        },

        editPlayer: function (id) {
            vm.mediator.publish('EditPlayerById', id);
        },

        notFound: function () {
            vm.mediator.publish('Show404View');
        }
    });
})(App.Players);