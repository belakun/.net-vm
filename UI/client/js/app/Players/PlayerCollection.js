'use strict';

(function (This) {
    This.PlayerCollection = Backbone.Collection.extend({
        url: '/OData/Players',

        model: This.Player,

        comparator: function (model) {
            return model.get('lastName');
        }
    });
})(App.Players);