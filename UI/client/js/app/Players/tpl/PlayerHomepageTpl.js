var playerHomepageTpl = _.template([
    '<div class="panel panel-default">',
        '<div class="panel-heading">',
            '<div class="row">',
                '<div class="col-sm-8">',
                    '<h4>Информация об игроке</h4>',
                '</div>',
                '<div class="col-sm-4">',
                    '<button class="btn btn-success pull-right edit">',
                        '<i class="glyphicon glyphicon-edit"></i> Редактировать',
                    '</button>',
                '</div>',
            '</div>',
        '</div>',


            '<div class="panel-body player-panel">',
                '<div class="row">',
                    '<div class="col-md-9 player-info-container">',
                        '<div class="form-group">',
                            '<label>Имя:</label></span>',
                            '<p class="grey" name="firstName"><%= player.firstName %></p>',
                         '</div>',
                         '<div class="form-group">',
                            '<label>Фамилия:</label>',
                            '<p class="grey" name="lastName"><%= player.lastName %></p>',
                        '</div>',
                         '<div class="form-group">',
                             '<label>Год рождения:</label>',
                             '<p class="grey" name="birthYear"><%= player.birthYear %></p>',
                        '</div>',
                         '<div class="form-group">',
                             '<label>Команда:</label>',
                             '<p class="grey" name="teamId"><%= team.name %></p>',
                        '</div>',
                        '<div class="form-group">',
                            '<label>Рост, см:</label>',
                            '<p class="grey" name="height"><%= player.height %></p>',
                        '</div>',
                        '<div class="form-group">',
                            '<label>Вес, кг:</label>',
                            '<p class="grey" name="weight"><%= player.weight %></p>',
                        '</div>',
                    '</div>',
                    '<div class="col-md-3 player-logo-container">',
                        '<i class="glyphicon glyphicon-player img-circle"></i>',
                    '</div>',
                '</div>',
                '<div class="row">',
                    '<div class="col-md-12 player-info-container">',
                        '<button type="button" class="btn btn-warning pull-left cancel">',
                            '<i class="glyphicon glyphicon-arrow-left"></i> Назад',
                        '</button>',
                        '<button type="button" class="btn btn-danger pull-right delete">',
                            '<i class="glyphicon glyphicon-remove"></i> Удалить',
                        '</button>',
                    '</div>',
                '</div>',
            '</div>',


    '</div>'
].join(""));