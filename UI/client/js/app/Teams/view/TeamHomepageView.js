"use strict";

(function (This) {
    This.TeamHomepageView = Backbone.View.extend({
        tagName: 'div',
        className: 'homepage',
        name: '',

        template: teamHomepageTpl,

        events: {
            'click .cancel': 'cancel',
            'click .edit': 'edit',
            'click .delete': 'confirmDelete'
        },

        initialize: function (options) {
            this.name = options.name;
            this.collection = new App.Players.PlayerCollection();
            //this.collection.on('add', this.addOne, this);
            this.collection.fetch();
        },

        render: function () {
            this.$el.append(this.template(this.model.toJSON()));
            //this.$el.append(this.template());

            return this;
        },

        addOne: function (player) {
            if (this.model.attributes.name === player.attributes.name) {
                //var a = player.attributes.name;
                //this.$('.players of this team').append(a);

                var view = new App.Players.PlayerView({model: player});
                this.$('.players of this team').append(view.render().el);
            }
        },

        cancel: function () {
            vm.mediator.publish('ShowTeams');
        },

        edit: function () {
            this.remove();

            vm.mediator.publish('EditTeam', this.model);
        },

        confirmDelete: function () {
            vm.mediator.publish('Popup', 'Вы действительно хотите удалить профиль команды?', this.delete.bind(this));
        },

        delete: function () {
            this.model.destroy();

            vm.mediator.publish('ShowTeams');

            vm.mediator.publish('Notice', 'success', ' успешно удален!');
        }
    });
})(App.Teams);