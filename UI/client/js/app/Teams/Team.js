'use strict';

(function (This) {
    This.Team = Backbone.Model.extend({
        urlRoot: '/OData/Teams',

        defaults: function () {
            return {
                'name':'',
                'captain':'',
                'coach':'',
                'achievements':''
            };
        }, 

        validation: {
            name: [{
                required: true,
                msg: 'Поле не может быть пустым'
            }, {
                maxLength: 60,
                msg: 'Поле не может содержать более 60 символов'
            }, {
                pattern: 'lettersOnly',
                msg: 'Поле должно содержать только буквы'
            }]
        }
    });
})(App.Teams);