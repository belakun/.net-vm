'use strict';

(function (This)  {
    This.Router = Backbone.Router.extend({
        routes: {
            'WebApi/Teams': 'getTeams',
            'WebApi/Teams/:id': 'getTeam',
            'WebApi/Teams*path': 'notFound'
        },
        
        initialize: function () {
            this.controller = new App.Teams.Controller();

            //URL navigation
            vm.mediator.subscribe('ShowTeams', this.navigateTeams, null, this);
            vm.mediator.subscribe('CreateTeam', this.navigateCreateTeam, null, this);
            vm.mediator.subscribe('EditTeam', this.navigateEditTeam, null, this);
            vm.mediator.subscribe('EditTeamById', this.navigateEditTeamById, null, this);
            vm.mediator.subscribe('ShowTeamInfo', this.navigateShowTeam, null, this);
            vm.mediator.subscribe('ShowTeamById', this.navigateShowTeamById, null, this);
            
            Backbone.history.loadUrl(Backbone.history.fragment);
        },

        navigateTeams: function () {
            this.navigate('WebApi/Teams');
        },

        navigateCreateTeam: function () {
            this.navigate('WebApi/Teams/new');
        },

        navigateEditTeam: function (team) {
            this.navigate('WebApi/Teams/' + team.id + '/edit');
        },

        navigateEditTeamById: function (teamId) {
            this.navigate('WebApi/Teams/' + teamId + '/edit');
        },

        navigateShowTeam: function (team) {
            this.navigate('WebApi/Teams/' + team.id);
        },

        navigateShowTeamById: function (id) {
            this.navigate('WebApi/Teams/' + id);
        },

        getTeams: function () {
            vm.mediator.publish('ShowTeams');
        },

        getTeam: function (id) {
            vm.mediator.publish('ShowTeamById', id);
        },

        notFound: function () {
            vm.mediator.publish('Show404View');
        }
    });
})(App.Teams);