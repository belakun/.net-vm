'use strict';

var App = {},
    vm = {};

setUp(App, ['Menu', 'Messenger', 'Tournaments', 'Users', 'Players', 'ServicePages', 'Navigator', 'About', 'Teams', 'ContextMenu']);
setUp(vm, ['mediator', 'messenger', 'navigator', 'servicePages', 'subRouters', 'router', 'teams']);

$(function () {
    vm.mediator = new Mediator();
    vm.messenger = new App.Messenger.Controller();
    vm.navigator = new App.Navigator.Controller();
    vm.router = new App.Router();
    vm.menu = new App.Menu.Controller();
    vm.contextMenu = new App.ContextMenu.Controller();
    vm.servicePages = new App.ServicePages.Controller();
	vm.subRouters = {};
    
    Backbone.history.start({pushState: true});
});